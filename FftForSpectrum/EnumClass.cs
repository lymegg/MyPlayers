﻿
namespace FftForSpectrum
{
    public enum BarCountModel
    {
        AUTO = 0,
        NUM = 1
    }

    public enum ShowType
    {
        BAR = 0,
        LINE = 1
    }
}
