﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyPlayers
{
    public class C_DownloadLyr
    {
        /// <summary>
        /// 根据歌曲名称的查找地址
        /// </summary>
        public static readonly string SearchPath_song = "http://geci.me/api/lyric/{0}";

        /// <summary>
        /// 根据歌曲名称和歌手名称的查找地址
        /// </summary>
        public static readonly string SearchPath_songAndartist = "http://geci.me/api/lyric/{0}/{1}";

        /// <summary>
        /// 根据歌曲id的查找地址
        /// </summary>
        public static readonly string SearchPath_songid = "http://geci.me/api/lyric/{0}";

        /// <summary>
        /// 根据专辑id的查找专辑封面地址
        /// </summary>
        public static readonly string SearchPath_album_id = "http://geci.me/api/lyric/{0}";

        
    }

    /// <summary>
    /// 歌词迷返回的搜索歌词的json对象
    /// </summary>
    public class SearchInfo_gecime
    {
        /// <summary>
        /// 结果数量
        /// </summary>
        public int count { get; set; }

        /// <summary>
        /// 应该是查询执行结果，为0应该是成功
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 歌词结果列表
        /// </summary>
        public List<LyrInfo_gecime> result { get; set; }
    }

    /// <summary>
    /// 歌词迷返回的单个歌词的json对象
    /// </summary>
    public class LyrInfo_gecime
    {
        /// <summary>
        /// 应该是专辑id
        /// </summary>
        public int aid { get; set; }

        /// <summary>
        /// 歌词文件下载地址
        /// </summary>
        public string lrc { get; set; }

        /// <summary>
        /// 歌手名称
        /// </summary>
        public string artist { get; set; }

        /// <summary>
        /// 歌名
        /// </summary>
        public string song { get; set; }

        /// <summary>
        /// 应该是歌曲id
        /// </summary>
        public int sid { get; set; }
    }
}
