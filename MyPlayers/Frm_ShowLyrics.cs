﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using libZPlay;
using C_DownloadLyr;

namespace MyPlayers
{
    public partial class Frm_ShowLyrics : Form
    {
        C_DownloadLyr.C_DownloadLyr aDownloadLyr = null;

        public Frm_ShowLyrics()
        {
            InitializeComponent();
            if (aDownloadLyr == null)
            {
                aDownloadLyr = new C_DownloadLyr.C_DownloadLyr();
            }
        }

        public void ReadLyricFile(string FileName, TID3InfoEx Mp3Info, bool OnLineSearch)
        {
            string iFilePath = Application.StartupPath + "\\LRC\\" + FileName + ".lrc";
            this.lyrControl1.Clear("正在寻找歌词……");
            if (File.Exists(iFilePath))
            {
                lyrControl1.ReadLyricForFile(iFilePath);
                return;
            }
            if (aDownloadLyr != null)
            {
                if (Mp3Info.Artist == "" && Mp3Info.Title == "")
                {
                    this.lyrControl1.Clear("ID3信息为空……");
                    return;
                }
                try
                {
                    bool iRet = aDownloadLyr.FindLrcAndDownLoadOne(Mp3Info.Title, iFilePath);
                    if (iRet)
                    {
                        lyrControl1.ReadLyricForFile(iFilePath);
                    }
                    else
                    {
                        this.lyrControl1.Clear(Mp3Info.Artist + " - " + Mp3Info.Title);
                    }
                }
                catch
                {
                    return;
                }
            }

        }

        public void ReadLyricFile(string LyrFilePath)
        {
            if (File.Exists(LyrFilePath))
            {
                lyrControl1.ReadLyricForFile(LyrFilePath);
                return;
            }
            else
            {
                this.lyrControl1.Clear("指定的歌词文件不存在……");
            }

        }

        public void SetCurrentLyric(string TimeStr)
        {
            lyrControl1.SetCurrentLyric(TimeStr);
        }

        public void SetCurrentLyric(Int32 Time)
        {
            lyrControl1.SetCurrentLyric(Time);
        }

        private void Frm_ShowLyrics_DragDrop(object sender, DragEventArgs e)
        {
            string ifilepath = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            if (Path.GetExtension(ifilepath).ToLower() == ".lrc")
            {
                string iFilePath = Application.StartupPath + "\\LRC\\" + C_PlayInfo.MusicName + ".lrc";
                File.Copy(ifilepath, iFilePath, true);
                this.ReadLyricFile(iFilePath);
            }
        }

        private void Frm_ShowLyrics_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
    }
}
