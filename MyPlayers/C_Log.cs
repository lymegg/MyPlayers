﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using C_Log;

namespace MyPlayers
{
    public class C_Log
    {
        private static LogManager aLogObj = new LogManager();

        private static bool aIsLog = true;

        /// <summary>
        /// 记录日志信息
        /// </summary>
        /// <param name="logmsg">日志内容</param>
        /// <param name="loglevel">日志等级</param>
        public static void log(string logmsg,LogLevel loglevel)
        {
            if (aIsLog)
            {
                aLogObj.WriteLog(loglevel, logmsg);
            }
        }
    }

}
