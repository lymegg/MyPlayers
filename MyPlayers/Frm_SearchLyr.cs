﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using C_DownloadLyr;
using libZPlay;

namespace MyPlayers
{
    public partial class Frm_SearchLyr : Form
    {
        private string aFilesPath;

        private Frm_ShowLyrics aFrm_ShowLyrics = null;

        public Frm_SearchLyr()
        {
            InitializeComponent();
        }

        public Frm_SearchLyr(TID3Info ID3Info, string FilesPath,Frm_ShowLyrics frmobj)
        {
            InitializeComponent();
            txt_title.Text = ID3Info.Title;
            txt_artist.Text = ID3Info.Artist;
            aFilesPath = FilesPath;
            aFrm_ShowLyrics = frmobj;
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            dataT_lyrs.Clear();
            string isong = txt_title.Text.Trim();
            string iartist = txt_artist.Text.Trim();
            if (isong == "" && iartist == "")
            {
                MessageBox.Show("歌曲名称不能为空");
                txt_title.Focus();
                return;
            }
            C_DownloadLyr.C_DownloadLyr iDownloadObj = new C_DownloadLyr.C_DownloadLyr();
            C_DownloadLyr.SearchLrcRet iSearchRetObj = null;
            if (iartist == "")
            {
                iSearchRetObj = iDownloadObj.SearchLrc(isong);
            }
            else
            {
                iSearchRetObj = iDownloadObj.SearchLrc(isong, iartist);
            }

            iDownloadObj.UpdataArtistName(iSearchRetObj.result);
            foreach(C_DownloadLyr.LrcAddObj iLyrObj in iSearchRetObj.result)
            {

                dataT_lyrs.Rows.Add(new object[] { iLyrObj.aid, iLyrObj.lrc, iLyrObj.artist, iLyrObj.song, iLyrObj.sid });
            }
        }

        private void dataGV_lrcs_DoubleClick(object sender, EventArgs e)
        {
            if (dataGV_lrcs.CurrentRow == null)
            {
                return;
            }

            string iFilePath = Application.StartupPath + "\\LRC\\" + Path.GetFileNameWithoutExtension(aFilesPath) + ".lrc";
            C_DownloadLyr.C_DownloadLyr iDownloadObj = new C_DownloadLyr.C_DownloadLyr();
            iDownloadObj.DownloadLrc(dataGV_lrcs.CurrentRow.Cells[1].Value.ToString(), iFilePath);
            if (aFrm_ShowLyrics != null)
            {
                aFrm_ShowLyrics.ReadLyricFile(iFilePath);
            }
        }
    }
}
