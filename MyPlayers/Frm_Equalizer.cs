﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MyPlayers
{
    public partial class Frm_Equalizer : Form
    {
        public event BGValueChangedEvenHandler BGValueChanged;
        public event PGValueChangedEvenHandler PGValueChanged;
        public event EQEnabledChangeEvenHandler EQEnabledChange;

        #region 均衡器预设

        /// <summary>
        /// 普通
        /// </summary>
        private readonly Int32[] PRESET_NORMAL = new Int32[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        /// <summary>
        /// 语音
        /// </summary>
        private readonly Int32[] PRESET_VOICE = new Int32[]{ -4, 0, 2, 1, 0, 0, 0, 0, -4, -6 };

        /// <summary>
        /// 布鲁斯
        /// </summary>
        private readonly Int32[] PRESET_BLUES = new Int32[] { -2, 0, 2, 1, 0, 0, 0, 0, -2, -4 };

        /// <summary>
        /// 电子乐
        /// </summary>
        private readonly Int32[] PRESET_ELECTRON = new Int32[] { -6, 1, 4, -2, -2, -4, 0, 0, 6, 6 };

        /// <summary>
        /// 歌剧
        /// </summary>
        private readonly Int32[] PRESET_OPERA = new Int32[] { 0, 0, 0, 4, 5, 3, 6, 3, 0, 0 };

        /// <summary>
        /// 古典乐
        /// </summary>
        private readonly Int32[] PRESET_CLASSICAL = new Int32[] { 0, 8, 8, 4, 0, 0, 0, 0, 2, 2 };

        /// <summary>
        /// 怀旧音乐
        /// </summary>
        private readonly Int32[] PRESET_REMINISCENCE = new Int32[] { -4, 0, 2, 1, 0, 0, 0, 0, -4, -6 };

        /// <summary>
        /// 金属乐
        /// </summary>
        private readonly Int32[] PRESET_METAL = new Int32[] { -6, 0, 0, 0, 0, 0, 4, 0, 4, 0 };

        /// <summary>
        /// 爵士乐
        /// </summary>
        private readonly Int32[] PRESET_JAZZ = new Int32[] { 0, 0, 0, 4, 4, 4, 0, 2, 3, 4 };

        /// <summary>
        /// 流行乐
        /// </summary>
        private readonly Int32[] PRESET_POP = new Int32[] { 3, 1, 0, -2, -4, -4, -2, 0, 1, 2 };

        /// <summary>
        /// 舞曲
        /// </summary>
        private readonly Int32[] PRESET_DANCE = new Int32[] { -2, 3, 4, 1, -2, -2, 0, 0, 4, 4 };

        /// <summary>
        /// 乡村音乐
        /// </summary>
        private readonly Int32[] PRESET_COUNTRY = new Int32[] { -2, 0, 0, 2, 2, 0, 0, 0, 4, 4 };

        /// <summary>
        /// 摇滚
        /// </summary>
        private readonly Int32[] PRESET_ROCK = new Int32[] { -2, 0, 2, 4, -2, -2, 0, 0, 4, 4 };

        #endregion

        public Frm_Equalizer()
        {
            InitializeComponent();
            //C_IrisSkin.AddForm(this);
        }

        public void InitInfo()
        {
            this.checkB_Enabled.Checked = C_PlayInfo.UseEqualizer;
            if (C_PlayInfo.EqualizerInfo == null)
            {
                C_PlayInfo.EqualizerInfo = new C_Equalizer();
            }
            this.vSBar_BG1.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[0];
            this.vSBar_BG10.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[9];
            this.vSBar_BG2.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[1];
            this.vSBar_BG3.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[2];
            this.vSBar_BG4.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[3];
            this.vSBar_BG5.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[4];
            this.vSBar_BG6.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[5];
            this.vSBar_BG7.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[6];
            this.vSBar_BG8.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[7];
            this.vSBar_BG9.Value = C_PlayInfo.EqualizerInfo.Equalizer_BG[8];
            this.vSBar_PG.Value = C_PlayInfo.EqualizerInfo.Equalizer_PG;
        }

        private void checkB_Enabled_CheckedChanged(object sender, EventArgs e)
        {
            if (EQEnabledChange != null)
            {
                EQEnabledChange(new EQEnabledChangeEventArgs(checkB_Enabled.Checked));
            }
        }

        private void vSBar_PG_ValueChanged(object sender, EventArgs e)
        {
            if (PGValueChanged != null)
            {
                PGValueChanged(new PGValueChangeEventArgs(vSBar_PG.Value));
            }
        }

        private void vSBar_BG_ValueChanged(object sender, EventArgs e)
        {
            if (BGValueChanged != null)
            {
                VScrollBar iBar = (VScrollBar)sender;
                Int32 iBGIndex = Int32.Parse(iBar.Tag.ToString());
                BGValueChanged(new BGValueChangeEventArgs(iBGIndex, iBar.Value));
            }
            
        }

        private void Menu_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem iMenuItem = (ToolStripMenuItem)sender;
            foreach (ToolStripMenuItem iMenuObj in cMenu_Preset.Items)
            {
                if (iMenuObj != iMenuItem)
                {
                    iMenuObj.Checked = false;
                }
            }
            
            SetBeforehandInfos(iMenuItem.Tag.ToString());
        }

        private void SetBeforehandInfos(String PresetName)
        {
            switch (PresetName.ToUpper())
            {
                case "NORMAL":
                    {
                        SetBeforehandInfo(PRESET_NORMAL);
                        break;
                    }
                case "VOICE":
                    {
                        SetBeforehandInfo(PRESET_VOICE);
                        break;
                    }
                case "BLUES":
                    {
                        SetBeforehandInfo(PRESET_BLUES);
                        break;
                    }
                case "ELECTRON":
                    {
                        SetBeforehandInfo(PRESET_ELECTRON);
                        break;
                    }
                case "OPERA":
                    {
                        SetBeforehandInfo(PRESET_OPERA);
                        break;
                    }
                case "CLASSICAL":
                    {
                        SetBeforehandInfo(PRESET_CLASSICAL);
                        break;
                    }
                case "REMINISCENCE":
                    {
                        SetBeforehandInfo(PRESET_REMINISCENCE);
                        break;
                    }
                case "METAL":
                    {
                        SetBeforehandInfo(PRESET_METAL);
                        break;
                    }
                case "JAZZ":
                    {
                        SetBeforehandInfo(PRESET_JAZZ);
                        break;
                    }
                case "POP":
                    {
                        SetBeforehandInfo(PRESET_POP);
                        break;
                    }
                case "DANCE":
                    {
                        SetBeforehandInfo(PRESET_DANCE);
                        break;
                    }
                case "COUNTRY":
                    {
                        SetBeforehandInfo(PRESET_COUNTRY);
                        break;
                    }
                case "ROCK":
                    {
                        SetBeforehandInfo(PRESET_ROCK);
                        break;
                    }
            }
        }

        private void SetBeforehandInfo(Int32[] PresetInfo)
        {
            if (PresetInfo.Length < 10)
            {
                return;
            }
            this.vSBar_BG1.Value = PresetInfo[0];
            this.vSBar_BG2.Value = PresetInfo[1];
            this.vSBar_BG3.Value = PresetInfo[2];
            this.vSBar_BG4.Value = PresetInfo[3];
            this.vSBar_BG5.Value = PresetInfo[4];
            this.vSBar_BG6.Value = PresetInfo[5];
            this.vSBar_BG7.Value = PresetInfo[6];
            this.vSBar_BG8.Value = PresetInfo[7];
            this.vSBar_BG9.Value = PresetInfo[8];
            this.vSBar_BG10.Value = PresetInfo[9];
        }

        private void btn_Preset_Click(object sender, EventArgs e)
        {
            this.cMenu_Preset.Show(btn_Preset, new Point(0, btn_Preset.Height));
        }
    }
}
