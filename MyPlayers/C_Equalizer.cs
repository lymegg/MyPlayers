﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyPlayers
{
    [Serializable]
    public class C_Equalizer
    {
        public const Int32 Equalizer_Max = 12;
        public const Int32 Equalizer_Min = -12;

        public Int32 Equalizer_PG = 0;
        public Int32[] Equalizer_BG = new Int32[10];
    }
}
