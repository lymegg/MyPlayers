﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyPlayers
{
    public delegate void BGValueChangedEvenHandler(BGValueChangeEventArgs EvenInfo);
    public delegate void PGValueChangedEvenHandler(PGValueChangeEventArgs EvenInfo);
    public delegate void EQEnabledChangeEvenHandler(EQEnabledChangeEventArgs EvenInfo);

    public class BGValueChangeEventArgs : EventArgs
    {
        private Int32 aBGIndex = 0;
        private Int32 aBGValue = 0;

        public BGValueChangeEventArgs(Int32 BGIndex,Int32 BGValue)
        {
            if ((BGIndex < 0) || (BGIndex > 10))
            {
                aBGIndex = 0;
            }
            else
            {
                aBGIndex = BGIndex;
            }
            if ((BGValue < -20) || (BGValue > 20))
            {
                aBGValue = 0;
            }
            else
            {
                aBGValue = BGValue;
            }
        }

        /// <summary>
        /// 顺序号
        /// </summary>
        public Int32 BGIndex
        {
            get { return aBGIndex; }
        }

        /// <summary>
        /// 当前值
        /// </summary>
        public Int32 BGValue
        {
            get { return aBGValue; }
        }
    }

    public class PGValueChangeEventArgs : EventArgs
    {
        private Int32 aPGValue = 0;

        public PGValueChangeEventArgs(Int32 PGValue)
        {
            if ((PGValue < -20) || (PGValue > 20))
            {
                aPGValue = 0;
            }
            else
            {
                aPGValue = PGValue;
            }
        }

        /// <summary>
        /// 当前值
        /// </summary>
        public Int32 PGValue
        {
            get { return aPGValue; }
        }
    }

    public class EQEnabledChangeEventArgs : EventArgs
    {
        private Boolean aEnabled = false;

        public EQEnabledChangeEventArgs(Boolean Enabled)
        {
            aEnabled = Enabled;
        }

        /// <summary>
        /// 是否启动均衡器
        /// </summary>
        public Boolean Enabled
        {
            get { return aEnabled; }
        }
    }
}
