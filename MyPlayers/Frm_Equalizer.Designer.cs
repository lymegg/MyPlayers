﻿namespace MyPlayers
{
    partial class Frm_Equalizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.vSBar_BG2 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG3 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG4 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG6 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG7 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG8 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG10 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG9 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG5 = new System.Windows.Forms.VScrollBar();
            this.vSBar_BG1 = new System.Windows.Forms.VScrollBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.vSBar_PG = new System.Windows.Forms.VScrollBar();
            this.checkB_Enabled = new System.Windows.Forms.CheckBox();
            this.btn_Preset = new System.Windows.Forms.Button();
            this.cMenu_Preset = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Menu_NORMAL = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Pop = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Rock = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_METAL = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Dance = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_ELECTRON = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_COUNTRY = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_JAZZ = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Classical = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_BLUES = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_REMINISCENCE = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_OPERA = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_VOICE = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.cMenu_Preset.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.vSBar_BG2);
            this.groupBox1.Controls.Add(this.vSBar_BG3);
            this.groupBox1.Controls.Add(this.vSBar_BG4);
            this.groupBox1.Controls.Add(this.vSBar_BG6);
            this.groupBox1.Controls.Add(this.vSBar_BG7);
            this.groupBox1.Controls.Add(this.vSBar_BG8);
            this.groupBox1.Controls.Add(this.vSBar_BG10);
            this.groupBox1.Controls.Add(this.vSBar_BG9);
            this.groupBox1.Controls.Add(this.vSBar_BG5);
            this.groupBox1.Controls.Add(this.vSBar_BG1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.vSBar_PG);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(407, 130);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "EQ";
            // 
            // vSBar_BG2
            // 
            this.vSBar_BG2.LargeChange = 1;
            this.vSBar_BG2.Location = new System.Drawing.Point(115, 17);
            this.vSBar_BG2.Maximum = 12;
            this.vSBar_BG2.Minimum = -12;
            this.vSBar_BG2.Name = "vSBar_BG2";
            this.vSBar_BG2.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG2.TabIndex = 27;
            this.vSBar_BG2.Tag = "1";
            this.vSBar_BG2.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG3
            // 
            this.vSBar_BG3.LargeChange = 1;
            this.vSBar_BG3.Location = new System.Drawing.Point(148, 17);
            this.vSBar_BG3.Maximum = 12;
            this.vSBar_BG3.Minimum = -12;
            this.vSBar_BG3.Name = "vSBar_BG3";
            this.vSBar_BG3.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG3.TabIndex = 26;
            this.vSBar_BG3.Tag = "2";
            this.vSBar_BG3.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG4
            // 
            this.vSBar_BG4.LargeChange = 1;
            this.vSBar_BG4.Location = new System.Drawing.Point(181, 17);
            this.vSBar_BG4.Maximum = 12;
            this.vSBar_BG4.Minimum = -12;
            this.vSBar_BG4.Name = "vSBar_BG4";
            this.vSBar_BG4.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG4.TabIndex = 25;
            this.vSBar_BG4.Tag = "3";
            this.vSBar_BG4.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG6
            // 
            this.vSBar_BG6.LargeChange = 1;
            this.vSBar_BG6.Location = new System.Drawing.Point(247, 17);
            this.vSBar_BG6.Maximum = 12;
            this.vSBar_BG6.Minimum = -12;
            this.vSBar_BG6.Name = "vSBar_BG6";
            this.vSBar_BG6.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG6.TabIndex = 24;
            this.vSBar_BG6.Tag = "5";
            this.vSBar_BG6.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG7
            // 
            this.vSBar_BG7.LargeChange = 1;
            this.vSBar_BG7.Location = new System.Drawing.Point(280, 17);
            this.vSBar_BG7.Maximum = 12;
            this.vSBar_BG7.Minimum = -12;
            this.vSBar_BG7.Name = "vSBar_BG7";
            this.vSBar_BG7.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG7.TabIndex = 23;
            this.vSBar_BG7.Tag = "6";
            this.vSBar_BG7.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG8
            // 
            this.vSBar_BG8.LargeChange = 1;
            this.vSBar_BG8.Location = new System.Drawing.Point(313, 17);
            this.vSBar_BG8.Maximum = 12;
            this.vSBar_BG8.Minimum = -12;
            this.vSBar_BG8.Name = "vSBar_BG8";
            this.vSBar_BG8.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG8.TabIndex = 22;
            this.vSBar_BG8.Tag = "7";
            this.vSBar_BG8.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG10
            // 
            this.vSBar_BG10.LargeChange = 1;
            this.vSBar_BG10.Location = new System.Drawing.Point(379, 17);
            this.vSBar_BG10.Maximum = 12;
            this.vSBar_BG10.Minimum = -12;
            this.vSBar_BG10.Name = "vSBar_BG10";
            this.vSBar_BG10.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG10.TabIndex = 21;
            this.vSBar_BG10.Tag = "9";
            this.vSBar_BG10.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG9
            // 
            this.vSBar_BG9.LargeChange = 1;
            this.vSBar_BG9.Location = new System.Drawing.Point(346, 17);
            this.vSBar_BG9.Maximum = 12;
            this.vSBar_BG9.Minimum = -12;
            this.vSBar_BG9.Name = "vSBar_BG9";
            this.vSBar_BG9.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG9.TabIndex = 20;
            this.vSBar_BG9.Tag = "8";
            this.vSBar_BG9.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG5
            // 
            this.vSBar_BG5.LargeChange = 1;
            this.vSBar_BG5.Location = new System.Drawing.Point(214, 17);
            this.vSBar_BG5.Maximum = 12;
            this.vSBar_BG5.Minimum = -12;
            this.vSBar_BG5.Name = "vSBar_BG5";
            this.vSBar_BG5.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG5.TabIndex = 19;
            this.vSBar_BG5.Tag = "4";
            this.vSBar_BG5.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // vSBar_BG1
            // 
            this.vSBar_BG1.LargeChange = 1;
            this.vSBar_BG1.Location = new System.Drawing.Point(82, 17);
            this.vSBar_BG1.Maximum = 12;
            this.vSBar_BG1.Minimum = -12;
            this.vSBar_BG1.Name = "vSBar_BG1";
            this.vSBar_BG1.Size = new System.Drawing.Size(17, 102);
            this.vSBar_BG1.TabIndex = 18;
            this.vSBar_BG1.Tag = "0";
            this.vSBar_BG1.ValueChanged += new System.EventHandler(this.vSBar_BG_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 17;
            this.label3.Text = "-20";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "20";
            // 
            // vSBar_PG
            // 
            this.vSBar_PG.LargeChange = 1;
            this.vSBar_PG.Location = new System.Drawing.Point(10, 17);
            this.vSBar_PG.Maximum = 12;
            this.vSBar_PG.Minimum = -12;
            this.vSBar_PG.Name = "vSBar_PG";
            this.vSBar_PG.Size = new System.Drawing.Size(17, 102);
            this.vSBar_PG.TabIndex = 14;
            this.vSBar_PG.ValueChanged += new System.EventHandler(this.vSBar_PG_ValueChanged);
            // 
            // checkB_Enabled
            // 
            this.checkB_Enabled.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkB_Enabled.AutoSize = true;
            this.checkB_Enabled.Location = new System.Drawing.Point(22, 157);
            this.checkB_Enabled.Name = "checkB_Enabled";
            this.checkB_Enabled.Size = new System.Drawing.Size(51, 22);
            this.checkB_Enabled.TabIndex = 15;
            this.checkB_Enabled.Text = "启  动";
            this.checkB_Enabled.UseVisualStyleBackColor = true;
            this.checkB_Enabled.CheckedChanged += new System.EventHandler(this.checkB_Enabled_CheckedChanged);
            // 
            // btn_Preset
            // 
            this.btn_Preset.Location = new System.Drawing.Point(102, 157);
            this.btn_Preset.Name = "btn_Preset";
            this.btn_Preset.Size = new System.Drawing.Size(51, 22);
            this.btn_Preset.TabIndex = 16;
            this.btn_Preset.TabStop = false;
            this.btn_Preset.Text = "预  设";
            this.btn_Preset.UseVisualStyleBackColor = true;
            this.btn_Preset.Click += new System.EventHandler(this.btn_Preset_Click);
            // 
            // cMenu_Preset
            // 
            this.cMenu_Preset.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_NORMAL,
            this.Menu_Pop,
            this.Menu_Rock,
            this.Menu_METAL,
            this.Menu_Dance,
            this.Menu_ELECTRON,
            this.Menu_COUNTRY,
            this.Menu_JAZZ,
            this.Menu_Classical,
            this.Menu_BLUES,
            this.Menu_REMINISCENCE,
            this.Menu_OPERA,
            this.Menu_VOICE});
            this.cMenu_Preset.Name = "cMenu_Preset";
            this.cMenu_Preset.ShowCheckMargin = true;
            this.cMenu_Preset.ShowImageMargin = false;
            this.cMenu_Preset.Size = new System.Drawing.Size(119, 290);
            // 
            // Menu_NORMAL
            // 
            this.Menu_NORMAL.CheckOnClick = true;
            this.Menu_NORMAL.Name = "Menu_NORMAL";
            this.Menu_NORMAL.Size = new System.Drawing.Size(118, 22);
            this.Menu_NORMAL.Tag = "NORMAL";
            this.Menu_NORMAL.Text = "普通";
            this.Menu_NORMAL.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_Pop
            // 
            this.Menu_Pop.CheckOnClick = true;
            this.Menu_Pop.Name = "Menu_Pop";
            this.Menu_Pop.Size = new System.Drawing.Size(118, 22);
            this.Menu_Pop.Tag = "POP";
            this.Menu_Pop.Text = "流行";
            this.Menu_Pop.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_Rock
            // 
            this.Menu_Rock.CheckOnClick = true;
            this.Menu_Rock.Name = "Menu_Rock";
            this.Menu_Rock.Size = new System.Drawing.Size(118, 22);
            this.Menu_Rock.Tag = "ROCK";
            this.Menu_Rock.Text = "摇滚";
            this.Menu_Rock.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_METAL
            // 
            this.Menu_METAL.CheckOnClick = true;
            this.Menu_METAL.Name = "Menu_METAL";
            this.Menu_METAL.Size = new System.Drawing.Size(118, 22);
            this.Menu_METAL.Tag = "METAL";
            this.Menu_METAL.Text = "金属乐";
            this.Menu_METAL.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_Dance
            // 
            this.Menu_Dance.CheckOnClick = true;
            this.Menu_Dance.Name = "Menu_Dance";
            this.Menu_Dance.Size = new System.Drawing.Size(118, 22);
            this.Menu_Dance.Tag = "DANCE";
            this.Menu_Dance.Text = "舞曲";
            this.Menu_Dance.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_ELECTRON
            // 
            this.Menu_ELECTRON.CheckOnClick = true;
            this.Menu_ELECTRON.Name = "Menu_ELECTRON";
            this.Menu_ELECTRON.Size = new System.Drawing.Size(118, 22);
            this.Menu_ELECTRON.Tag = "ELECTRON";
            this.Menu_ELECTRON.Text = "电子乐";
            this.Menu_ELECTRON.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_COUNTRY
            // 
            this.Menu_COUNTRY.CheckOnClick = true;
            this.Menu_COUNTRY.Name = "Menu_COUNTRY";
            this.Menu_COUNTRY.Size = new System.Drawing.Size(118, 22);
            this.Menu_COUNTRY.Tag = "COUNTRY";
            this.Menu_COUNTRY.Text = "乡村音乐";
            this.Menu_COUNTRY.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_JAZZ
            // 
            this.Menu_JAZZ.CheckOnClick = true;
            this.Menu_JAZZ.Name = "Menu_JAZZ";
            this.Menu_JAZZ.Size = new System.Drawing.Size(118, 22);
            this.Menu_JAZZ.Tag = "JAZZ";
            this.Menu_JAZZ.Text = "爵士乐";
            this.Menu_JAZZ.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_Classical
            // 
            this.Menu_Classical.CheckOnClick = true;
            this.Menu_Classical.Name = "Menu_Classical";
            this.Menu_Classical.Size = new System.Drawing.Size(118, 22);
            this.Menu_Classical.Tag = "CLASSICAL";
            this.Menu_Classical.Text = "古典";
            this.Menu_Classical.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_BLUES
            // 
            this.Menu_BLUES.CheckOnClick = true;
            this.Menu_BLUES.Name = "Menu_BLUES";
            this.Menu_BLUES.Size = new System.Drawing.Size(118, 22);
            this.Menu_BLUES.Tag = "BLUES";
            this.Menu_BLUES.Text = "布鲁斯";
            this.Menu_BLUES.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_REMINISCENCE
            // 
            this.Menu_REMINISCENCE.CheckOnClick = true;
            this.Menu_REMINISCENCE.Name = "Menu_REMINISCENCE";
            this.Menu_REMINISCENCE.Size = new System.Drawing.Size(118, 22);
            this.Menu_REMINISCENCE.Tag = "REMINISCENCE";
            this.Menu_REMINISCENCE.Text = "怀旧音乐";
            this.Menu_REMINISCENCE.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_OPERA
            // 
            this.Menu_OPERA.CheckOnClick = true;
            this.Menu_OPERA.Name = "Menu_OPERA";
            this.Menu_OPERA.Size = new System.Drawing.Size(118, 22);
            this.Menu_OPERA.Tag = "OPERA";
            this.Menu_OPERA.Text = "歌剧";
            this.Menu_OPERA.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Menu_VOICE
            // 
            this.Menu_VOICE.CheckOnClick = true;
            this.Menu_VOICE.Name = "Menu_VOICE";
            this.Menu_VOICE.Size = new System.Drawing.Size(118, 22);
            this.Menu_VOICE.Tag = "VOICE";
            this.Menu_VOICE.Text = "语音";
            this.Menu_VOICE.Click += new System.EventHandler(this.Menu_Click);
            // 
            // Frm_Equalizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 191);
            this.Controls.Add(this.btn_Preset);
            this.Controls.Add(this.checkB_Enabled);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Frm_Equalizer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "均衡器";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.cMenu_Preset.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.VScrollBar vSBar_BG2;
        private System.Windows.Forms.VScrollBar vSBar_BG3;
        private System.Windows.Forms.VScrollBar vSBar_BG4;
        private System.Windows.Forms.VScrollBar vSBar_BG6;
        private System.Windows.Forms.VScrollBar vSBar_BG7;
        private System.Windows.Forms.VScrollBar vSBar_BG8;
        private System.Windows.Forms.VScrollBar vSBar_BG10;
        private System.Windows.Forms.VScrollBar vSBar_BG9;
        private System.Windows.Forms.VScrollBar vSBar_BG5;
        private System.Windows.Forms.VScrollBar vSBar_BG1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.VScrollBar vSBar_PG;
        private System.Windows.Forms.CheckBox checkB_Enabled;
        private System.Windows.Forms.Button btn_Preset;
        private System.Windows.Forms.ContextMenuStrip cMenu_Preset;
        private System.Windows.Forms.ToolStripMenuItem Menu_NORMAL;
        private System.Windows.Forms.ToolStripMenuItem Menu_Classical;
        private System.Windows.Forms.ToolStripMenuItem Menu_METAL;
        private System.Windows.Forms.ToolStripMenuItem Menu_Dance;
        private System.Windows.Forms.ToolStripMenuItem Menu_ELECTRON;
        private System.Windows.Forms.ToolStripMenuItem Menu_COUNTRY;
        private System.Windows.Forms.ToolStripMenuItem Menu_JAZZ;
        private System.Windows.Forms.ToolStripMenuItem Menu_BLUES;
        private System.Windows.Forms.ToolStripMenuItem Menu_REMINISCENCE;
        private System.Windows.Forms.ToolStripMenuItem Menu_OPERA;
        private System.Windows.Forms.ToolStripMenuItem Menu_Pop;
        private System.Windows.Forms.ToolStripMenuItem Menu_VOICE;
        private System.Windows.Forms.ToolStripMenuItem Menu_Rock;

    }
}